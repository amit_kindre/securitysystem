`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Amit Kindre
// 
// Create Date: 21.01.2019 17:49:47
// Design Name: 
// Module Name: DecimalToBcdEncoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DecimalToBcdEncoder(din,y,debug_led1,debug_led2,debug_led3,debug_led4,debug_led5,
debug_led6,debug_led7,debug_led8,debug_led9,debug_led10);
    input [9:0] din;
    output reg [3:0] y;
    output reg  debug_led1,debug_led2,debug_led3,debug_led4,debug_led5,debug_led6,debug_led7,debug_led8,debug_led9,debug_led10;
    always @ (din)
    begin
        case(din)
            10'b0000000001:
            begin
                y=4'b0000;
                debug_led1 = 1;
            end
            10'b0000000010:
            begin
                y=4'b0001;
                debug_led2 = 1;
            end
            10'b0000000100:
            begin
                y=4'b0010;
                debug_led3 = 1;
            end
            10'b0000001000:
            begin
                y=4'b0011;
                debug_led4 = 1;
            end
            10'b0000010000:
            begin
                y=4'b0100;
                debug_led5 = 1;
            end
            10'b0000100000:
            begin
                y=4'b0101;
                debug_led6 = 1;
            end
            10'b0001000000:
            begin
                y=4'b0110;
                debug_led7 = 1;
            end
            10'b0010000000:
            begin
                y=4'b0111;
                debug_led8 = 1;
            end
            10'b0100000000:
            begin
                y=4'b1000;
                debug_led9 = 1;
            end
            10'b1000000000:
            begin
                y=4'b1001;
                debug_led10 = 1;
            end
            default:y=4'b0000;
            
        endcase
        
    end
endmodule
