`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.01.2019 00:54:48
// Design Name: 
// Module Name: MagnitudeComparator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MagnitudeComparator(A,B,Eq);
    input [0:3] A;
    input [0:3] B;
    output reg Eq;
    
    always @ (A or B)
    begin
        Eq <= (A == B) ? 1'b1 : 1'b0; 
    
    end
endmodule
