`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.01.2019 02:01:32
// Design Name: 
// Module Name: OneShot
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module OneShot(Enable,clk,QOut);
    input Enable,clk;
    output reg QOut;

    always @ (posedge clk)
    begin
        if(Enable)
        begin
             QOut = 1'b1;
             #2 QOut = 1'b0;
        end    
    end
endmodule
