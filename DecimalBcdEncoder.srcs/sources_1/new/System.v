`timescale 1ns / 1ps



module System(sw,enterKey,alarm,clk,rst,an,seg,dp);
    input [9:0] sw;
    input clk;
    input rst;
    input enterKey;
    output alarm;
    
    output [3:0] an;  // anode signals of the 7-segment LED display
    output [6:0] seg;          // cathode patterns of the 7-segment LED display
    output dp;
    
    wire [3:0] tempBcdOut;
    wire [3:0] tempShiftAout;
    wire [3:0] tempShiftBout;
    wire [3:0] tempCodeLogic;
    wire tempEq,tempOrOut,tempTimeoutA,tempTimeoutB;
    wire [7:0] tempPasslengthData= 8'b 00010000;
    //parameter passwordLength = 8'b00010000;
    
   // wire [7:0] tempPasslengthData = passwordLength ;
    
    
    or(tempOrOut,sw,enterKey);
    codeSelectionLogic codesel (tempCodeLogic,tempOrOut);
    shiftRegister shiftra (tempBcdOut,tempTimeoutA,tempShiftAout);
    shiftRegister shiftrb (tempCodeLogic,tempTimeoutA,tempShiftBout);
    MagnitudeComparator magcomp (tempShiftAout,tempShiftBout,tempEq);
    
    DecimalToBcdEncoder dbencoder(sw,tempBcdOut);
    OneShot onesb (tempTimeoutA,clk,tempTimeoutB);
    OneShot onesa (tempOrOut,clk,tempTimeoutA);
    serialShiftRegister ssr(8'b00010000,tempTimeoutB,tempEq,alarm);
    
    SevenSegment display(clk,tempBcdOut,seg,an,dp);
    
endmodule
