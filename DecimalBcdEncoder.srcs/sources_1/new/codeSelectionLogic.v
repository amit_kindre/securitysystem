`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.01.2019 01:36:01
// Design Name: 
// Module Name: codeSelectionLogic
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module codeSelectionLogic(Bout,clkin,shld);
    output reg [3:0] Bout;
    input clkin;
    input shld;
    
    parameter 
        digit0 = 4'b0001,
        digit1 = 4'b0010,
        digit2 = 4'b0100,
        digit3 = 4'b1000,
        digit4 = 4'b1000;
        reg[3:0] currentDigit,nextDigit;
        
        
    always @ (posedge clkin)
    begin
        if(shld)
        begin
            currentDigit <= nextDigit;
            Bout = currentDigit;
        end
        else
        begin
            currentDigit <= digit0;
            Bout = currentDigit;
        end
    end
    
    always @ (currentDigit)
    begin
        case(currentDigit)
        digit0:nextDigit <= digit1;
        digit1:nextDigit <= digit2;
        digit2:nextDigit <= digit3;
        digit3:nextDigit <= digit4;
        digit4:nextDigit <= digit1;
        default: nextDigit <= digit1;
        endcase
    end
    
endmodule
