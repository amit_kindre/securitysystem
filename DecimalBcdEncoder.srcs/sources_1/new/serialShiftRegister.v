`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.01.2019 18:45:35
// Design Name: 
// Module Name: serialShiftRegister
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module serialShiftRegister(din,clk,shld,dout);
    input [7:0] din;
    input clk;
    input shld;
    output reg dout;
    reg [7:0] temp;
    always @ (posedge clk) 
    begin 
    if (shld) 
        temp <= din;
     else 
     begin 
        dout <= temp[0]; 
        temp <= {1'b0, temp[7:1]}; 
        end 
     end   
endmodule

